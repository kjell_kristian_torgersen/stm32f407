#include <stdint.h>

#include "stm32f4xx_hal.h"

extern TIM_HandleTypeDef htim6;

void delay_init(void)
{
	HAL_TIM_Base_Start(&htim6);
}

void delay_us (uint32_t us)
{
    __HAL_TIM_SET_COUNTER(&htim6,0);
    while ((__HAL_TIM_GET_COUNTER(&htim6)) < us);
}

void delay_ms (uint32_t ms)
{
	//HAL_Delay(ms);
	for(uint32_t i = 0; i < ms; i++) {
		delay_us(1000);
	}
}

