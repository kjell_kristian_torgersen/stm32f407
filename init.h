/*
 * init.h
 *
 *  Created on: Oct 22, 2020
 *      Author: kjell
 */

#ifndef STM32_IMPLEMENTATION_INIT_H_
#define STM32_IMPLEMENTATION_INIT_H_

#include "api/i2c.h"

i2c_t * i2c_init(void);

#endif /* STM32_IMPLEMENTATION_INIT_H_ */
