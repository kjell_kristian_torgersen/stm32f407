#include "stm32f4xx_hal.h"

#include "api/i2c.h"

struct i2c {
	I2C_HandleTypeDef * handle;
};

extern I2C_HandleTypeDef hi2c1;

static i2c_t i2c;

i2c_t * i2c_init(void)
{
	i2c.handle = &hi2c1;
	return &i2c;
}

bool i2c_deviceIsReady(i2c_t * this, uint8_t address, uint8_t trials) {
	return HAL_I2C_IsDeviceReady(&hi2c1, address, trials, HAL_MAX_DELAY) == HAL_OK;
}

sz_t i2c_read(i2c_t * this, uint8_t address, void * buf, sz_t count)
{
	//if(HAL_I2C_IsDeviceReady(this->handle, address << 1, 3, 10) == HAL_OK) {
		if (HAL_I2C_Master_Receive(this->handle, address<<1, buf, count, HAL_MAX_DELAY) != HAL_OK)
		{
			return 0;
		}
	//}
	return count;
}

sz_t i2c_write(i2c_t * this, uint8_t address, const void * buf, sz_t count)
{
	//if(HAL_I2C_IsDeviceReady(this->handle, address << 1, 3, 10) == HAL_OK) {
		if (HAL_I2C_Master_Transmit(this->handle, address<<1, (void*)buf, count, HAL_MAX_DELAY) != HAL_OK) {
			return 0;
		}
	//}
	return count;
}

bool i2c_scan(uint8_t addr)
{
	HAL_StatusTypeDef res = HAL_I2C_IsDeviceReady(i2c.handle, addr << 1, 3, 10);
	return res == HAL_OK;
}
