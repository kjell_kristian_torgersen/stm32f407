#include "stm32f4xx_hal.h"

#include "hal/uart.h"
#include "utils/ringbuf/ringbuf.h"

#include "utils/misc.h"

struct uart {
	UART_HandleTypeDef *huart;
	ringbuf_t rxbuf;
	ringbuf_t txbuf;
};

static uart_t uart;
static uint8_t rxbyte;

static void startTransmit(uart_t *this)
{
	HAL_UART_StateTypeDef state = HAL_UART_GetState(this->huart);
	if (!((state & HAL_UART_STATE_BUSY_TX) & 3)) {
		uint8_t b;
		ringbuf_read(&uart.rxbuf, &b, 1);
		HAL_UART_Transmit_IT(this->huart, &b, 1);
	}
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	if (ringbuf_used(&uart.txbuf)) {
		uint8_t b;
		ringbuf_read(&uart.txbuf, &b, 1);
		HAL_UART_Transmit_IT(huart, &b, 1);
	}

}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{

	ringbuf_write(&uart.rxbuf, &rxbyte, 1);
	HAL_UART_Receive_IT(huart, &rxbyte, 1);
}

uart_t* uart_init(UART_HandleTypeDef *huart, uint32_t baudRate, void *rxbuf, sz_t rxbufsize, void *txbuf, sz_t txbufsize)
{
	uart.huart = huart;
	ringbuf_init(&uart.rxbuf, rxbuf, rxbufsize);
	ringbuf_init(&uart.txbuf, txbuf, txbufsize);
	HAL_UART_Receive_IT(huart, &rxbyte, 1);
	return &uart;
}

sz_t uart_write(uart_t *this, const void *data, sz_t count)
{
	//HAL_UART_Transmit_IT(this->huart, (void*)data, count);

	sz_t n = ringbuf_write(&this->txbuf, data, count);
	startTransmit(this);
	return n;
}

sz_t uart_read(uart_t *this, void *data, sz_t count)
{
	return ringbuf_read(&this->rxbuf, data, count);
}
