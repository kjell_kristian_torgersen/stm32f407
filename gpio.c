#include "stm32f4xx_hal.h"

#include "api/gpio.h"

static GPIO_TypeDef * const gpios[] = {GPIOA, GPIOB, GPIOC, GPIOD, GPIOE, GPIOF, GPIOG, GPIOH, GPIOI};
static uint32_t modes[3] = {GPIO_MODE_INPUT, GPIO_MODE_OUTPUT_PP, GPIO_MODE_OUTPUT_OD};

bool gpio_getPin(uint8_t port, uint8_t pin)
{

	//gpio_togglePin(0,1);
	return HAL_GPIO_ReadPin(gpios[port], 1 << pin) ? true : false;
}

void gpio_config(uint8_t port, uint8_t pin, gpio_config_t config)
{
	GPIO_InitTypeDef init;
	init.Pin = 1 << pin;
	init.Mode = modes[config];
	init.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	init.Pull = GPIO_PULLUP;

	HAL_GPIO_Init(gpios[port], &init);
}

void gpio_togglePin(uint8_t port, uint8_t pin)
{
	HAL_GPIO_TogglePin(gpios[port], 1 << pin);
}

void gpio_setPin(uint8_t port, uint8_t pin)
{
	gpios[port]->BSRR = (1 << pin);
}

void gpio_clearPin(uint8_t port, uint8_t pin)
{
	gpios[port]->BSRR = (uint32_t)1 << (pin + 16);
}

void gpio_setValue(uint8_t port, uint8_t pin, bool value)
{
	gpios[port]->BSRR = value ? (1 << pin) : (1 << (pin + 16));
}

