#include "stm32f4xx_hal.h"

uint32_t timer_seconds = 0;

uint32_t timer_ms(void)
{
	return HAL_GetTick();
}

uint32_t timer_s(void)
{
	return timer_seconds;
}
