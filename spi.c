#include "api/spi.h"
#include "stm32f4xx_hal.h"

struct spi {
	SPI_HandleTypeDef * handle;
};

spi_t spi;

spi_t * spi_init(SPI_HandleTypeDef * handle)
{
	spi.handle = handle;
	HAL_SPI_Init(spi.handle);
	return &spi;
}

void spi_setMode(spi_t * this, spi_mode_t mode)
{
	// TODO: implement
}

sz_t spi_read(spi_t * this, void * buf, sz_t count)
{
	HAL_SPI_Receive(this->handle, buf, count, 100);
	return count;
}

sz_t spi_write(spi_t * this, const void * buf, sz_t count)
{
	HAL_SPI_Transmit(this->handle, (void*)buf, count, 100);
	return count;
}

sz_t spi_readwrite(spi_t * this, void * rxbuf, const void * txbuf, sz_t count)
{
	HAL_SPI_TransmitReceive(this->handle, (void*)txbuf, rxbuf, count, 100);
	return count;
}

void spi_close(spi_t * this)
{
	HAL_SPI_DeInit(this->handle);
}
